<div style="text-align:center">
<img src="http://gmarciani.com/data/media/brand/gmarciani-logo.svg" width="100">
<h1>Gmarciani.com</h1>
<p>Personal website about big data analytics, artificial intelligence and high performance computing.</p>
<p>Powered by Jekyll, Node.js, Gulp, Bower and many other cool stuff</p>
</div>

## Build
If you want to create a similar blog, this is how you can build and deploy it.

Install all requirements

```console
$ npm install
$ bower install
```

Build it

```console
$ gulp build
```

## Deploy
Now you can watch it locally

```console
$ gulp serve
```

And deploy it

```console
$ vagrant provision
```

## Authors
Giacomo Marciani, [gmarciani@acm.org](mailto:gmarciani@acm.org)

## Contributing
If you want to suggest me improvements or pointing out issues, please feel free to contact me.

## License
The project is released under the [MIT License](https://opensource.org/licenses/MIT).
