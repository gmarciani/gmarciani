/*******************************************************************************
* GMARCIANI
* Gulp Configuration
*******************************************************************************/

/*******************************************************************************
* PACKAGES
*******************************************************************************/

// Gulp
var gulp        = require('gulp');
var gutil       = require('gulp-util');
var plumber     = require('gulp-plumber');

// File Management
var concat      = require('gulp-concat');
var copy        = require('gulp-copy');
var rename      = require('gulp-rename');
var del         = require('del');

// Styles
var compass     = require('gulp-compass');
var scssLint    = require('gulp-scss-lint');
var cssLint     = require('gulp-csslint');
var cleanCss    = require('gulp-clean-css');

// Scripts
var jshint      = require('gulp-jshint');
var uglify      = require('gulp-uglify');

// Images
var svg2png     = require('gulp-svg2png');
var imageResize = require('gulp-image-resize');
var gm          = require('gulp-gm');

// Views
var pug         = require('gulp-pug');
var pugLint     = require('gulp-pug-lint');
var htmlmin     = require('gulp-htmlmin');
var sitemap     = require('gulp-sitemap');

// Other
var shell       = require('gulp-shell');
var child       = require('child_process');
var runSequence = require('run-sequence');
var filter      = require('gulp-filter');
var isWindows   = require('is-windows');
var isOSX       = require('is-osx');

/*******************************************************************************
* CONFIGURATIONS
*******************************************************************************/
var config = {
  url: 'http://gmarciani.com',
  images: {
    format: 'jpg',
    people: {
      size: 400
    }
  }
}

/*******************************************************************************
* PATHS
*******************************************************************************/

var paths = {

  base        : '.',

  src      : {
    base      : 'src',
    every     : 'src/**/*',

    scripts   : {
      base    : 'src/scripts',
      every   : 'src/scripts/**/*.js'
    },

    styles    : {
      base    : 'src/styles',
      every   : 'src/styles/**/*.{css,s+(a|c)ss}',
      main    : 'src/styles/main.scss'
    },

    fonts     : {
      base    : 'src/fonts',
      every   : 'src/fonts/**/*.{woff,otf,ttf,svg,eot}'
    },

    images    : {
      base    : 'src/images',
      every   : 'src/images/**/*.{svg,eps,png,jpg,ico}',
      bg : {
        base : 'src/images/bg',
        every: 'src/images/bg/**/*.{svg,eps,png,jpg,ico}'
      },
      brand   : {
        base    : 'src/images/brand',
        every   : 'src/images/brand/**/*.{svg,eps,png,jpg,jpeg,ico}',
        logo    : 'src/images/brand/logo.svg',
        favicon : 'src/images/brand/favicon.svg',
        icon    : 'src/images/brand/icon.svg',
        startup : 'src/images/brand/startup.svg',
        mask    : 'src/images/brand/mask.svg',
        fall    : 'src/images/brand/fallback.svg'
      }
    },

    views     : {
      base    : 'src/views',
      every   : 'src/views/**/*.pug',
      index   : 'src/views/index.pug'
    },

    meta     : {
      base    : 'src/meta',
      every   : 'src/meta/**/*'
    }
  },

  site        : {
    base      : 'site',
    every     : 'site/**/*',

    scripts   : {
      base    : 'site/assets/scripts',
      every   : 'site/assets/scripts/**/*.js',
      vendor  : 'site/assets/scripts/vendor',
      app     : {
        base  : 'site/assets/scripts',
        main  : 'site/assets/scripts/app.js'
      }
    },

    styles    : {
      base    : 'site/assets/styles',
      every   : 'site/assets/styles/**/*.css',
      main    : 'site/assets/styles/app.css'
    },

    images    : {
      base    : 'site/assets/images',
      every   : 'site/assets/images/**/*.{svg,eps,png,jpg,ico}',
      bg      : 'site/assets/images/bg',
      brand   : 'site/assets/images/brand'
    },

    fonts     : {
      base    : 'site/assets/fonts',
      every   : 'site/assets/fonts/**/*.{woff,otf,ttf,svg,eot}'
    },

    views     : {
      base    : 'site',
      every   : 'site/**/*.html',
      index   : 'site/index.html'
    }
  },

  tmp           : {
    sass        : '.sass-cache'
  }

}

/*******************************************************************************
* CLEAN
*******************************************************************************/
gulp.task('clean', function() {
  return del(
    [paths.site.base,
      paths.tmp.sass
    ]);
});

/*******************************************************************************
* BUILD
*******************************************************************************/
gulp.task('build', function() {
  runSequence(
    'views',
    'fonts',
    'images',
    ['scripts', 'styles'],
    'meta'
  );
});

/*******************************************************************************
* SERVE
*******************************************************************************/
gulp.task('serve', function() {
  gulp.start('watch');
});

gulp.task('connect', function() {
  browserSync.init({
    files: [paths.site.every],
    port: 4000,
    server: {
      baseDir: "_site"
    }
  });
});

gulp.task('watch', function() {
  gulp.watch(paths.src.views.every, ['views']);
  gulp.watch(paths.src.styles.every, ['styles']);
  gulp.watch(paths.src.scripts.every, ['scripts']);
  gulp.watch(paths.src.fonts.every, ['fonts']);
});

/*******************************************************************************
* VIEWS
*******************************************************************************/
gulp.task('views', function() {
  gulp.src(paths.src.views.index)
  .pipe(plumber())
  .pipe(pugLint())
  .pipe(pug({
      pretty: false
  }))
  .pipe(htmlmin({collapseWhitespace: true}))
  .pipe(gulp.dest(paths.site.views.base));
});

/*******************************************************************************
* SCRIPTS
*******************************************************************************/
gulp.task('scripts', function() {
  return gulp.src(paths.src.scripts.every)
  .pipe(plumber())
  .pipe(jshint('.jshint.json'))
  .pipe(jshint.reporter('default'))
  .pipe(concat('main.js'))
  .pipe(gulp.dest(paths.site.scripts.base))
  .pipe(uglify())
  .pipe(rename({
      suffix: '.min'
  }))
  .pipe(gulp.dest(paths.site.scripts.base));
});

/*******************************************************************************
* STYLES
*******************************************************************************/
gulp.task('styles', function() {
  return gulp.src(paths.src.styles.every)
  .pipe(plumber())
  .pipe(scssLint({
    'config': 'scsslint.yml'
  }))
  .pipe(compass({
    config_file: 'config.rb',
    sass: paths.src.styles.base,
    css: paths.site.styles.base
  }))
  .pipe(gulp.dest(paths.site.styles.base))
  .pipe(cleanCss())
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest(paths.site.styles.base));
});

/*******************************************************************************
* IMAGES
*******************************************************************************/
gulp.task('images', function() {
  gulp.start('images-bg');
  gulp.start('images-brand');
});

gulp.task('images-bg', function() {
  return gulp.src(paths.src.images.bg.every)
  .pipe(plumber())
  .pipe(gm(function (gmfile) {
    return gmfile.setFormat(config.images.format);
  }))
  .pipe(gulp.dest(paths.site.images.bg));
});

gulp.task('images-brand', function() {
  gulp.start('images-brand-logo');
  gulp.start('images-brand-favicons');
  gulp.start('images-brand-apple-icons');
  gulp.start('images-brand-google-icons');
  gulp.start('images-brand-microsoft-icons');
  gulp.start('images-brand-fallback');
});

gulp.task('images-brand-logo', function() {
  var logo = {name: 'logo', width: 500, height: 500};

  return gulp.src(paths.src.images.brand.logo)
  .pipe(gulp.dest(paths.site.images.brand))
  .pipe(plumber())
  .pipe(svg2png())
  .pipe(imageResize({
    width: logo.width,
    height: logo.height,
    crop: false,
    upscale: false,
    imageMagick: true
  }))
  .pipe(rename({
    basename: logo.name
  }))
  .pipe(gulp.dest(paths.site.images.brand));
});

gulp.task('images-brand-favicons', function() {
  var icons = [
    {name: 'favicon-64', width: 64, height: 64},
    {name: 'favicon-48', width: 48, height: 48},
    {name: 'favicon-32', width: 32, height: 32},
    {name: 'favicon-24', width: 24, height: 24},
    {name: 'favicon-16', width: 16, height: 16}
  ];

  var icosizes = [16, 24, 32, 48, 64];

  gulp.src(paths.src.images.brand.favicon)
  .pipe(plumber())
  .pipe(gulp.dest(paths.site.images.brand));

  gulp.src(paths.src.images.brand.favicon)
  .pipe(plumber())
  .pipe(shell(
    'convert <%= file.path %> -define icon:auto-resize='
    + icosizes.join(',') + ' '
    + paths.site.images.brand + '/favicon.ico'
  ));

  icons.forEach(function(image) {
    gulp.src(paths.src.images.brand.favicon)
    .pipe(plumber())
    .pipe(svg2png())
    .pipe(imageResize({
      width: image.width,
      height: image.height,
      crop: false,
      upscale: false,
      imageMagick: true
    }))
    .pipe(rename({
      basename: image.name
    }))
    .pipe(gulp.dest(paths.site.images.brand));
  });
});

gulp.task('images-brand-apple-icons', function() {
  var icons = [
    {name: 'apple-touch-icon-180', width: 180, height: 180},
    {name: 'apple-touch-icon-152', width: 152, height: 152},
    {name: 'apple-touch-icon-120', width: 120, height: 120},
    {name: 'apple-touch-icon-76', width: 76, height: 76},
    {name: 'apple-touch-icon-57', width: 57, height: 57}
  ];

  var startups = [
    {name: 'apple-touch-startup-image-320x480', width: 320, height: 480}
  ];

  icons.forEach(function(image) {
    gulp.src(paths.src.images.brand.icon)
    .pipe(plumber())
    .pipe(svg2png())
    .pipe(imageResize({
      width: image.width,
      height: image.height,
      crop: false,
      upscale: false,
      imageMagick: true
    }))
    .pipe(rename({
      basename: image.name
    }))
    .pipe(gulp.dest(paths.site.images.brand));
  });

  startups.forEach(function(image) {
    gulp.src(paths.src.images.brand.startup)
    .pipe(plumber())
    .pipe(svg2png())
    .pipe(imageResize({
      width: image.width,
      height: image.height,
      crop: false,
      upscale: false,
      imageMagick: true
    }))
    .pipe(rename({
      basename: image.name
    }))
    .pipe(gulp.dest(paths.site.images.brand));
  });

  gulp.src(paths.src.images.brand.mask)
  .pipe(plumber())
  .pipe(gulp.dest(paths.site.images.brand));
});

gulp.task('images-brand-google-icons', function() {
  var icons = [
    {name: 'google-icon-192', width: 192, height: 192},
    {name: 'google-icon-144', width: 144, height: 144},
    {name: 'google-icon-96', width: 96, height: 96},
    {name: 'google-icon-72', width: 72, height: 72},
    {name: 'google-icon-48', width: 48, height: 48},
    {name: 'google-icon-36', width: 36, height: 36}
  ];

  icons.forEach(function(image) {
    gulp.src(paths.src.images.brand.icon)
    .pipe(plumber())
    .pipe(svg2png())
    .pipe(imageResize({
      width: image.width,
      height: image.height,
      crop: false,
      upscale: false,
      imageMagick: true
    }))
    .pipe(rename({
      basename: image.name
    }))
    .pipe(gulp.dest(paths.site.images.brand));
  });
});

gulp.task('images-brand-microsoft-icons', function() {
  var icons = [
    {name: 'microsoft-tile-image-large', width: 310, height: 310},
    {name: 'microsoft-tile-image-medium', width: 150, height: 150},
    {name: 'microsoft-tile-image-small', width: 70, height: 70}
  ];

  icons.forEach(function(image) {
    gulp.src(paths.src.images.brand.icon)
    .pipe(plumber())
    .pipe(svg2png())
    .pipe(imageResize({
      width: image.width,
      height: image.height,
      crop: false,
      upscale: false,
      imageMagick: true
    }))
    .pipe(rename({
      basename: image.name
    }))
    .pipe(gulp.dest(paths.site.images.brand));
  });
});

gulp.task('images-brand-fallback', function () {
  return gulp.src(paths.src.images.brand.fall)
  .pipe(plumber())
  .pipe(gulp.dest(paths.site.images.brand));
});

/*******************************************************************************
* FONTS
*******************************************************************************/
gulp.task('fonts', function() {
  return gulp.src(paths.src.fonts.every)
  .pipe(plumber())
  .pipe(gulp.dest(paths.site.fonts.base));
});

/*******************************************************************************
* META
*******************************************************************************/
gulp.task('meta', function() {
  return gulp.src(paths.src.meta.every)
  .pipe(plumber())
  .pipe(gulp.dest(paths.site.base));
});

/*******************************************************************************
* PRODUCTION
*******************************************************************************/
gulp.task('production', function() {
  gulp.start('min-html');
  gulp.start('sitemap');
});

gulp.task('min-html', function() {
  return gulp.src(paths.site.views)
  .pipe(plumber())
  .pipe(htmlmin({collapseWhitespace: true}))
  .pipe(gulp.dest(paths.site.base));
});

gulp.task('sitemap', function() {
  return gulp.src(paths.site.views, { read: false })
  .pipe(plumber())
  .pipe(sitemap({
      siteUrl: config.url
  }))
  .pipe(gulp.dest(paths.site.base));
});

/*******************************************************************************
* UTILS
*******************************************************************************/
function message(scope, command, argument) {
  gutil.log(
    gutil.colors.cyan(scope), ':',
    gutil.colors.blue(command), ':',
    gutil.colors.yellow(argument)
  );
}

function printout(error, stdout, stderr) {
  console.log(stdout);
  console.log(stderr);
  if (error !== null) {
    console.log(stderr);
  }
}

gulp.task('check-platform', function() {
  if (isWindows()) {
    console.log('we are on Windows');
  } else if (isOSX()) {
    console.log('we are on OSX');
  } else {
    console.log('we are on Linux');
  }
});
